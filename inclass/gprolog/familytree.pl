mother_child(stephanie, jeremyjr).
 
father_child(jeremy, jaycob).
father_child(jeremy, jeremyjr).
father_child(jeremy, jymere).
 
sibling(X, Y)      :- parent_child(Z, X), parent_child(Z, Y).
 
parent_child(X, Y) :- father_child(X, Y).
parent_child(X, Y) :- mother_child(X, Y).
