/* Jeremy Moore
   Is it or is it not? Thats the question.*/
   
#include <stdio.h>

int main(){
int i = 100;
if (i > 50) goto START;
MIDDLE:
printf("I think it is\n");
goto FINISH;
START:
printf("Is this Spagetti Code?\n");
goto MIDDLE;
FINISH:
printf(";-)(-:\n");
}
