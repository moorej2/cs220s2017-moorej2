//*************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Jeremy Moore
// CMPSC 220
// Lab 4
// Date: 28 February 2017
//***********************************


public class recursiveF {

public static void main (String[] args)
{
  double h = 1.5; //these are test values
  System.out.println("h="+h);
  double u = 2.5; //these are test values
  myrec(h, u);
  for(double i =1; h<100; h++){ //go until 100 so it is not"infinetely recursed"
              System.out.println("EXAMPLE OF RECURSION: " + h);
         }
}

public static void myrec(double h, double u)
{
  System.out.println("u="+u);
}
}
