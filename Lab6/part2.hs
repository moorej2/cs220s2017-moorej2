//*************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Jeremy Moore, Brent Temeng, Andre Bryan
// CMPSC 220
// Lab 6
// Date: April 4 2017
//***********************************

data DayOfWeek
    = Sunday | Monday | Tuesday | Wednesday | Thursday | Friday | Saturday
    deriving (Eq, Enum, Bounded)

data Month
    = July deriving (Enum, Bounded, Show)

next :: (Eq a, Enum a, Bounded a) => a -> a
next x | x == maxBound = minBound
       | otherwise     = succ x

pad :: Int -> String
pad day = case show day of
    [c] -> [' ', c]
    cs  -> cs

month :: Month -> DayOfWeek -> Int -> String

month m startDay maxDay = show m ++ " 1776\n" ++ week ++ spaces Sunday
  where
    week = "Su  M Tu  W Th F  Sa       U.S Independence Month!!!\n"

    spaces currDay | startDay == currDay = days startDay 1
                   | otherwise           = "   " ++ spaces (next currDay)

    days Sunday    n | n > maxDay = "\n"
    days _         n | n > maxDay = "\n\n"
    days Saturday  n              = pad n ++ "\n" ++ days  Sunday    (succ n)
    days day       n              = pad n ++ " "  ++ days (next day) (succ n)

year = month July   Thursday  31


main = putStr year
