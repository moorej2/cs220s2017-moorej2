//*************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Jeremy Moore
// CMPSC 220
// Lab 1
// Date: 24 January 2017
//*************************************//

public class lab1Double {
    public static void main(String[] args) {
        double i = 5.0, j = 6.0, k = 127.0, l = 128.0, m = 255.0,
            n = 32767.0, o = 32768.0;
        System.out.println("i="+i);
        System.out.println("j="+j);
        System.out.println("k="+k);
        System.out.println("l="+l);
        System.out.println("m="+m);
        System.out.println("n="+n);
        System.out.println("o="+o);
    }
}
