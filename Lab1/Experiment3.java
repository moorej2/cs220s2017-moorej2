//*************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Jeremy Moore
// CMPSC 220
// Lab 1
// Date: 24 January 2017
//*************************************//
// This file was used for experimenting with arithmetic operations
public class Experiment3 {
    public static void main(String[] args) {
        int i = 5, j = 6, k = 127, l = 128, m = 255,
            n = 32767, o = 32768;
        int p = 500;

        System.out.println("i="+i %p);
        System.out.println("j="+j %p);
        System.out.println("k="+k %p);
        System.out.println("l="+l %p);
        System.out.println("m="+m %p);
        System.out.println("n="+n %p);
        System.out.println("o="+o %p);
    }
}
