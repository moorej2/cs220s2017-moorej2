//*************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Jeremy Moore
// CMPSC 220
// Lab 1
// Date: 24 January 2017
//*************************************//

public class Lab1 {
    public static void main(String[] args) {
        int i = 5, j = 6, k = 127, l = 128, m = 255,
            n = 32767, o = 32768;
        System.out.println("i="+i);
        System.out.println("j="+j);
        System.out.println("k="+k);
        System.out.println("l="+l);
        System.out.println("m="+m);
        System.out.println("n="+n);
        System.out.println("o="+o);
    }
}
