# Honor Code: The work I am submitting is a result of my own thinking and efforts.
# Jeremy Moore
# CMPSC 220
# Lab 5
# Date: 7 March 2017

"""
  name.py get the gator's name
"""

class gatorName:
    def __init__(self,name):
        self.name = name
    def getName(self):
        return self.name
    def speak(self):
        return "huge gator "
