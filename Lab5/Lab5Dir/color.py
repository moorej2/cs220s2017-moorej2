# Honor Code: The work I am submitting is a result of my own thinking and efforts.
# Jeremy Moore
# CMPSC 220
# Lab 5
# Date: 7 March 2017

"""
 color.py get the color of the gator and prints "that chomps"
"""


class gatorColor:
    def __init__(self,color):
        self.color = color
    def getName(self):
        return self.color
    def speak(self):
        return "that chomps!"
