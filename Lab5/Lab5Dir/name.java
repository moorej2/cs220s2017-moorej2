//*************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Jeremy Moore
// CMPSC 220
// Lab 5
// Date: 7 March 2017
//***********************************
import java.util.Scanner;


class gatorName {  // class that determines color asks user for input
   private String name; // name is a string variable

   // Constructor:
   public gatorName(String n) {
      name = n;
   }

   public String name() { return name; }

   public String is() { return "is a huge gator!"; }
}



class gatorColor { //class that determines color of first gator
   private String color; // color is a string variable

   // Constructor:
   public gatorColor(String c) {
      color = c;
   }

   public String color() { return color; }

   public String is() { return "Chomp!"; } // for print line "the gator chomps"
}



public class Lab5 { //this is the main method
   public static void main(String[] args) {
      Scanner in = new Scanner(System.in);
      String name;
      String color;

      System.out.print("What is the name of the gator? ");
      name = in.next();
      gatorName gator = new gatorName(name);

      System.out.print(gator.name()+"'s favorite color is? ");
      color = in.next();
      gatorColor favcolor = new gatorColor(color);
      System.out.println(gator.name()+"S "+gator.is());
      System.out.print(gator.name()+"'s color is , "+favcolor.color()+", he says ");
      System.out.println(favcolor.is());
      System.out.println(gator.name()+" has a twin, his twin's color is green");  //color of a second gator default
   }
}
