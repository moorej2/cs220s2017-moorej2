# Honor Code: The work I am submitting is a result of my own thinking and efforts.
# Jeremy Moore
# CMPSC 220
# Lab 5
# Date: 7 March 2017


from color import *
from name import *
"""
  main program that is the bridge between name.py and color.py
"""

if __name__ == "__main__":
   name = raw_input("What is the gator's name? ")
   names = gatorName(name)

   color = raw_input(names.getName()+"'s favorite color is? ")
   colors = gatorColor(color)

   print (names.getName()+" is a "+names.speak())
   print (names.getName()+"'s is a huge, "+colors.getName()+
           " gator, "+colors.speak())
   print(names.getName()+" also has a twin. His twin is green.")
